<?php
/*
Plugin Name: Dev Sales
Plugin URI: http://nickhaskins.com
Description: A sales plugin for PageLines Store Developers
Author: Nick Haskins
Author URI: http://www.nickhaskins.com
Version: 1.3
pagelines: true
*/

// Check for Framework
add_action('pagelines_setup', 'devsales_init' );
function devsales_init() {
	new BA_devSales;
}

// Go time
class BA_devSales {

	const version = '1.2';
	
	private $store;
	private $creds;

	// Construct
	function __construct() {

		$this->id = 'dev-sales';

		$this->url = sprintf('%s/%s', WP_PLUGIN_URL, $this->id);
		$this->dir = sprintf('%s/%s', WP_PLUGIN_DIR, $this->id);

		$this->creds = array(
			'user' => ploption('devsales_user'),
			'key'  => ploption('devsales_apikey')
		);
		$this->store = new PLStoreDevAPI( $this->creds['user'], $this->creds['key'] );

		$this->actions();
	}

	// Plugin Setup
	function actions() {

		// Load LESS
		add_action( 'template_redirect',        array(&$this, 'devsales_less' ));

		// Load Sidebar Widget
		add_action( 'widgets_init',             array(&$this, 'devsales_load_widgets') );

		// Setup Options
		add_action( 'init',                     array(&$this, 'setup_options') );

		// Load Dashboard Wiget
		add_action( 'wp_dashboard_setup',       array(&$this, 'dashboard_widget_init'));

		// Load Admin CSS
		add_action( 'admin_enqueue_scripts',    array(&$this, 'dash_style' ));
	}

	// Load Admin Style
	function dash_style() {
		wp_register_style( 'devsales_admin_css', $this->url.'/admin.css', true, self::version );
		wp_enqueue_style( 'devsales_admin_css' );
	}


	// Load Widgets
	function devsales_load_widgets() {

		// Load Widget Lib Files
		require_once('widgets/sales-widget.php');

		// Register Widgets
		register_widget('BA_Sales_Widget');

	}

	// LESS File
	function devsales_less() {

		if(function_exists('pagelines_insert_core_less'))
			$file = sprintf( '%sstyle.less', plugin_dir_path( __FILE__ ) );
			pagelines_insert_core_less( $file );

	}

	// Load Dashboard Widget
	function dashboard_widget_display() {

		extract( $this->creds );

		// quicker
		$store = $this->store;

		$product      = $store->validate_product( ploption('devsales_product') );
		$product_name = $store->get_product_info( $product, 'name' );
		$days         = ploption('devsales_days');

		if ( !$user || !$key ) {
			echo '<p><em>You need to provide your Developer API credientials before this thing will work!</em></p>';
			printf('<p><a href="%s">Click here</a> to fill in your account information.</p>', admin_url( PL_SETTINGS_URL .'#Dev_Sales' ));
			printf('Your developer API key can be found on the <a href="%s" target="_blank">API &amp; Payments</a> tab of your PageLines Launchpad account.',
				'https://www.pagelines.com/launchpad/member.php?tab=developer_api'
			);
			return;
		}

		$args = array(
			'product' => $product,
			'days'    => $days
		);
		$request = $product ? 'product-total' : 'developer-total';

		//plprint( $request, 'dashboard widget api request' );

		$response = $store->request( $request, $args );

		//plprint( $response, 'dashboard widget api response' );

		if ( ! is_array( $response ) ) {
			echo $response; // error message
			return;
		}
		else {
			$data     = $response;
			$total    = $data['total'];
			$takehome = $total * 0.7;
		}

		// no recorded sales yet
		if ( null === $total ) {
			echo '<p>No data currently available.</p>';
			return;
		}

		echo '<div class="ds-admin-left"><p class="ds-admin-head">Stats</p>';

			if ( $product ) {

				echo '<p class="ds-admin-totalsales"><span>'.count($data["items"]).'</span> Copies Sold</p>';
				echo '</div><div class="ds-admin-right"><p class="ds-admin-head">Sales</p>';
				printf('<p class="ds-admin-totalmoney"><span title="$%s Gross">$%s (Net) %s</span></p></div>', $data['total'], $takehome, $days ? "in Last $days days" : 'All time');

			} else {

				echo '<p class="ds-admin-totalsales"><span>'.$data["count"].'</span> Total Sales</p>';
				echo '</div><div class="ds-admin-right"><p class="ds-admin-head">Sales</p>';
				echo '<p class="ds-admin-totalmoney"><span>$'.$takehome.'</span> made this month</p></div>';

			}

			printf('<p class="ds-admin-moreinfo">Edit settings <a href="%s">here</a></p>', admin_url( PL_SETTINGS_URL .'#Dev_Sales' ));
			echo '<p class="ds-admin-contribute">Contribute to this plugin <a href="https://bitbucket.org/beardedavenger/dev-sales" target="_blank">here</a></p>';
	}

	function dashboard_widget_init() {

		extract( $this->creds );


		$title = sprintf('<img src="%s" class="devsales-title-icon" /> DevSales - ', PL_ADMIN_IMAGES . '/favicon-pagelines.png');

		if ( !$user || !$key ) {
			$title .= 'Uh oh...';
		}
		else {

			// quicker
			$store = $this->store;

			$product      = $store->validate_product( ploption('devsales_product') );
			$product_name = $store->get_product_info( $product, 'name' );
			$days         = ploption('devsales_days');

			$title        .= sprintf('Product Stats: %s', $product ? $product_name : 'All');
		}

		wp_add_dashboard_widget('devsales_widget', $title, array(&$this,'dashboard_widget_display'));
	}


	function setup_options() {

		$store = $this->store;
		$products = (array) $store->products;

		$options = array(
			'devsales_setup' => array(
				'type'          => 'multi_option',
				'title'         => __( 'Setup', 'devsales' ),
				'shortexp'      => __( 'Setup options', 'devsales' ),
				'selectvalues'  => array(
					'devsales_user' => array(
						'type'       => 'text',
						'inputlabel' => 'Username'
					),
					'devsales_apikey' => array(
						'type'       => 'apikey',
						'inputlabel' => 'API Key'
					),
				),
			),
			'devsales_product_setup' => array(
				'type'          => 'multi_option',
				'title'         => __( 'Product Options', 'devsales' ),
				'shortexp'      => __( 'Product options', 'devsales' ),
				'selectvalues'  => array(
					'devsales_product' => array(
						'type'         => !empty($products) ? 'select' : 'text',
						'inputlabel'   => 'Product',
						'selectvalues' => $products
					),
					'devsales_days' => array(
						'type'       => 'text_small',
						'inputlabel' => 'Days'
					),
					'devsales_getalltime' => array(
						'type'       => 'check',
						'inputlabel' => 'Get all time sales'
					),
				),
			),
			'devsales_moreinfo'    => array(
				'type'     => '',
				'title'    => __('<strong style="display:block;font-size:16px;color:#eaeaea;text-shadow:0 1px 0 black;padding:7px 7px 5px;background:#333;margin-top:5px;border-radius:3px;border:1px solid white;letter-spacing:0.1em;box-shadow:inset 0 0 3px black;">HOW TO USE:</strong>','devsales'),
				'shortexp' => __('<span>Visit the <a href="/wp-admin/index.php">dashboard</a> to see your widget.</span><br/><br /><strong style="display:block;">Username</strong> Input your Launchpad username.<br/><br />
								<strong style="display:block;">API Key</strong> Get your API key in Launchpad, under API tab.<br/><br />
								<strong style="display:block;">Product</strong> Enter the slug of your product. If you do not enter a product, it will revert to show all sales for all products. <br/><br />
								<strong style="display:block;">Days</strong> Here you can enter the number of days to go back. If you leave this field blank, it will automatically retrieve the current months stats. If you wish to see all sales since the beginning of time, tick the option for "Get all time sales."<br/><br />
								<strong style="display:block;">FYI</strong> The totals shown are as accurate as the API can get us, and are shown with PageLines 30% commission already taken out. The price of your product, is only accurate back to 6 months.
				','devsales'),
			),

		);

		pl_add_options_page(
			array(
				'icon' => $this->url.'/icon.png',
				'name' => 'Dev_Sales',
				'array' => $options
			)
		);
	}

}
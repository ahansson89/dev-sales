== Description ==
A dev sales Wordpress plugin for PageLines Developers who sell items on the PageLines Store. Plugin includes a Wordpress dashboard widget, and front-end widget. Setup options under Site Options-->DevSales.


== Changelog ==

= 1.3 =
* Updated to use Store API 1.1

= 1.2 =
* If days are left blank, dashboard widget auto updates with each new day
* Added option to get all time sales
* Edit link fixed

= 1.1 =
* Added total sales

= 1.0 =
* Initial release